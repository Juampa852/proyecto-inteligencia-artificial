#!/usr/bin/python
from fann2 import libfann
from glob import glob
from PIL import Image

def evaluar(imagen):
	#Cargamos la red neuronal ya entrenada
	ann = libfann.neural_net()
	ann.create_from_file("rna2.net")
	#Abrimos la imágen
	nueva  = Image.open(imagen)
	#Redimensionamos la imágen a 50x50px y lo pasamos a una lista, donde cada elemto es un píxel [r,g,b]
	nueva = nueva.resize((50,50))
	arreglo = list(nueva.getdata())
	tmep = []
	#Vectorizamos completamente el arreglo de la imágen
	for x in arreglo:
		tmep.append(x[0])
		tmep.append(x[1])
		tmep.append(x[2])
	tmep.append(50)#Indica que la imágen es de 50x50px, para que la red vaya obteniendo los valores de 50 en 50
	return ann.run(tmep)
